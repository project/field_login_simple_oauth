<?php

namespace Drupal\field_login_simple_oauth\Repository;

use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\user\UserAuthInterface;

/**
 * The user repository.
 */
final class UserRepository implements UserRepositoryInterface {

  /**
   * Constructs a UserAuth object.
   *
   * @param \Drupal\user\UserAuthInterface $userAuth
   *   The original user authentication service.
   */
  public function __construct(
    protected UserAuthInterface $userAuth,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getUserEntityByUserCredentials($username, $password) {
    $uid = $this->userAuth->authenticate($username, $password);

    if ($uid) {
      // Create a user entity.
      $user = new UserEntity();
      $user->setIdentifier($uid);

      return $user;
    }
    return NULL;
  }

}
