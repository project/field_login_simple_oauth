<?php

namespace Drupal\field_login_simple_oauth\Repository;

use League\OAuth2\Server\Repositories\RepositoryInterface;

/**
 * The user repository Interface.
 */
interface UserRepositoryInterface extends RepositoryInterface {

  /**
   * Get a user entity.
   *
   * @param string $username
   *   The user name to authenticate.
   * @param string $password
   *   A plain-text password, such as trimmed text from form values.
   *
   * @return \Drupal\simple_oauth\Entities\UserEntity|null
   *   The UserEntity on success, or NULL on failure to authenticate.
   */
  public function getUserEntityByUserCredentials(
    string $username,
    string $password,
  );

}
