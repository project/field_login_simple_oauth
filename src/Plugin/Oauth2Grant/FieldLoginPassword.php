<?php

declare(strict_types=1);

namespace Drupal\field_login_simple_oauth\Plugin\Oauth2Grant;

use Drupal\consumers\Entity\Consumer;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field_login_simple_oauth\Grant\FieldLoginPasswordGrant;
use Drupal\field_login_simple_oauth\Repository\UserRepositoryInterface;
use Drupal\simple_oauth\Plugin\Oauth2GrantBase;
use League\OAuth2\Server\Grant\GrantTypeInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Psr\Container\ContainerInterface;

/**
 * The field login password grant plugin.
 *
 * @Oauth2Grant(
 *  id = "field_login_password",
 *  label = @Translation("Field Login Password")
 * )
 */
class FieldLoginPassword extends Oauth2GrantBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected UserRepositoryInterface $userRepository,
    protected RefreshTokenRepositoryInterface $refreshTokenRepository,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('field_login_simple_oauth.repositories.user'),
      $container->get('simple_oauth.repositories.refresh_token'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getGrantType(Consumer $client): GrantTypeInterface {
    $grant = new FieldLoginPasswordGrant($this->userRepository, $this->refreshTokenRepository);

    $refreshTokenTTL = !$client->get('refresh_token_expiration')->isEmpty ? $client->get('refresh_token_expiration')->value : 1209600;
    $duration = new \DateInterval(sprintf('PT%dS', $refreshTokenTTL));
    $grant->setRefreshTokenTTL($duration);

    return $grant;
  }

}
