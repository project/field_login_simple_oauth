<?php

declare(strict_types=1);

namespace Drupal\field_login_simple_oauth\Grant;

use DateInterval;
use Drupal\field_login_simple_oauth\Repository\UserRepositoryInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\RequestAccessTokenEvent;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\RequestRefreshTokenEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Field Login password grant class.
 */
class FieldLoginPasswordGrant extends AbstractGrant {

  /**
   * UserRepository constructor.
   *
   * @var \Drupal\field_login_simple_oauth\Repository\UserRepositoryInterface
   */
  protected $userRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    UserRepositoryInterface $userRepository,
    RefreshTokenRepositoryInterface $refreshTokenRepository,
  ) {
    $this->setFieldUserRepository($userRepository);
    $this->setRefreshTokenRepository($refreshTokenRepository);
    $this->refreshTokenTTL = new DateInterval('P1M');
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldUserRepository(UserRepositoryInterface $userRepository) {
    $this->userRepository = $userRepository;
  }

  /**
   * {@inheritdoc}
   */
  public function respondToAccessTokenRequest(
    ServerRequestInterface $request,
    ResponseTypeInterface $responseType,
    DateInterval $accessTokenTTL
  ) {
    // Validate request.
    $client = $this->validateClient($request);
    $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
    $user = $this->validateUser($request);

    // Finalize the requested scopes.
    $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

    // Issue and persist new access token.
    $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
    $this->getEmitter()->emit(new RequestAccessTokenEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request, $accessToken));
    $responseType->setAccessToken($accessToken);

    // Issue and persist new refresh token if given.
    $refreshToken = $this->issueRefreshToken($accessToken);

    if ($refreshToken !== NULL) {
      $this->getEmitter()->emit(new RequestRefreshTokenEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request, $refreshToken));
      $responseType->setRefreshToken($refreshToken);
    }

    return $responseType;
  }

  /**
   * Validate user by google credentials.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   Psr\Http\Message\ServerRequestInterface.
   *
   * @return \League\OAuth2\Server\Entities\UserEntityInterface
   *   League\OAuth2\Server\Entities\UserEntityInterface.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *    When there is no user.
   */
  protected function validateUser(ServerRequestInterface $request) {
    $username = $this->getRequestParameter('username', $request);
    if (!\is_string($username)) {
      throw OAuthServerException::invalidRequest('username');
    }

    $password = $this->getRequestParameter('password', $request);
    if (!\is_string($password)) {
      throw OAuthServerException::invalidRequest('password');
    }

    return $this->userRepository->getUserEntityByUserCredentials(
      $username,
      $password,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier(): string {
    return 'field_login_password';
  }

}
