# Field Login & Simple Oauth Password Grant

This module implements the integration of the [simple_oauth](https://www.drupal.org/project/simple_oauth) module
with the [field_login](https://www.drupal.org/project/field_login) module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/field_login_simple_oauth).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/field_login_simple_oauth).

## Usage

To use this module, simply enable the **Field Login Password** grant type in your OAuth2 Consumer.
You can then obtain an access token by requesting it with the following payload:

```json
{
  "grant_type": "field_login_password",
  "client_id": "__your-client-id__",
  "client_secret": "__your-client-secret__",
  "username": "drupal_user_field_value",
  "password": "drupal_password"
}
```

## Important

The username is based on the values of the enabled fields in the field_login settings,
for example: UID, Name, Email, Phone, ID number.

## Maintainers

- xiang Gao - [qiutuo](https://www.drupal.org/u/qiutuo)
